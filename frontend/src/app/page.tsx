import Link from "next/link";

const Home = () => {
  return (
    <main>
      <br />
      <div className="flex flex-col flex-nowrap justify-center">
        <h1 className="underline decoration-sky-500 text-center">
          Página para probar CI/CD (probando cambio 1)
        </h1>
      </div>
    </main>
  );
};

export default Home;
